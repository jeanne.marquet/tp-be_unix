#include "utils.h"
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>



int main(int argc, char** argv) {
  
  int socket_connexion;                //On configure le socket client 
  socket_connexion = socket(AF_INET, SOCK_STREAM, 0) ; //AF_INET pour IPV4, SOCK_STREAM pour TCP 
  if (socket_connexion <0 ) {
    exit_msg("socket connexion", 1 ) ; 
    }
  //On configure l'adresse de connexion pour se connecter au serveur
  struct sockaddr_in address_serveur ;
  address_serveur.sin_family = AF_INET ; 
  address_serveur.sin_addr.s_addr = inet_addr(SERVER_IP);
  address_serveur.sin_port = htons(SERVER_PORT) ; 
  if (connect(socket_connexion, (struct sockaddr*) &address_serveur, sizeof(address_serveur))<0)  {
    exit_msg("connexion client", 1) ; //Gestion de l'erreur 
  }
  
  //On envoie une requête au serveur 
  char buffer_socket [LINE_SIZE] ;  
  fgets(buffer_socket, LINE_SIZE, stdin) ; 
  printf("%s \n", buffer_socket ) ; 
  if(send(socket_connexion, buffer_socket, strlen(buffer_socket)+1, 0)<0){ exit_msg("error send", 1) ; }  
  char reply [REPLY_SIZE] ; 
  if (recv(socket_connexion,reply , sizeof(reply), 0) <0 ){exit_msg("reply error" , 1) ; } // On récupère une répo
  fputs(reply, stdout) ; 
  return 0;
}
