#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "bdd.h"
#include "utils.h"
#include <semaphore.h>

//Nom du fichier contenant les données
static const char* DATA = "data";

//variable globale sémaphore
sem_t* semaphore ; 

//Retourne une string à partir d'un Day 
char* day_to_string(enum Day d) {
  switch (d) {
    case MON: return "Lundi";
    case TUE: return "Mardi";
    case WED: return "Mercredi";
    case THU: return "Jeudi";
    case FRI: return "Vendredi";
    case SAT: return "Samedi";
    case SUN: return "Dimanche";
    case NONE: return "Not a day";
  }
}

//Retourne un Day à partir d'un string
//dans le cas où la string ne correspond pas à un jour, on renvoie NONE
enum Day string_to_day(char* dd) {
  char d[LINE_SIZE]; 
  strcpy(d, dd);
  //Conversion en minuscule
  for (int i = 0; i < strlen(d); i++) 
    d[i] = tolower(d[i]);
  
  if (strcmp("lundi", d) == 0) return MON;
  else if (strcmp("mardi", d) == 0) return TUE;
  else if (strcmp("mercredi", d) == 0) return WED;
  else if (strcmp("jeudi", d) == 0) return THU;
  else if (strcmp("vendredi", d) == 0) return FRI;
  else if (strcmp("samedi", d) == 0) return SAT;
  else if (strcmp("dimanche", d) == 0) return SUN;
  else return NONE;
}


// Libère la mémoire d'un pointeur vers Data
void data_free(Data* d) {
  free(d->name);
  free(d->activity);
  free(d);
}

//Modifie une chaîne de caratère correspondant à data
void data_format(char* l, Data* data) {
  sprintf(l, "%s,%s,%s,%d\n", 
      data->name, data->activity, 
      day_to_string(data->day), data->hour);
}


//Retourne une structure Data à partir d'une ligne de donnée
// get_data("toto,arc,lundi,4") ->  Data { "toto", "arc", MON, 4 };
// Attention il faudra libérer la mémoire vous-même après avoir utilisé
// le pointeur généré par cette fonction
Data* get_data(char* line) {
  char* parse;
  Data* data = malloc(sizeof(Data));
  char error_msg[LINE_SIZE];
  sprintf(error_msg, "Erreur de parsing pour: %s\n", line);
  
  //On s'assure que la ligne qu'on parse soit dans le mémoire autorisée en 
  // écriture
  char* l = malloc(strlen(line)+1);
  l = strncpy(l, line, strlen(line)+1);

  parse = strtok(l, ",");
  if (parse == NULL) exit_msg(error_msg, 0);
  data->name = malloc(strlen(parse)+1);
  strcpy(data->name, parse);

  parse = strtok(NULL, ",");
  if (parse == NULL) exit_msg(error_msg, 0);
  data->activity = malloc(strlen(parse)+1);
  strcpy(data->activity, parse);

  parse = strtok(NULL, ",");
  if (parse == NULL) exit_msg(error_msg, 0);
  data->day = string_to_day(parse);

  parse = strtok(NULL, "\n");
  if (parse == NULL) exit_msg(error_msg, 0);
  data->hour = atoi(parse);
  free(l); 

  return data;
  }



//La fonction _add_data_  retourne 0 si l'opération s'est bien déroulé
//sinon -1
int add_data(Data* data) {//à chaque fois que j'appelle add data, un sémamphore doit être pris
      FILE *fichier; // Déclarer un pointeur de type FILE pour le fichier

   // Ouvrir le fichier en mode écriture ("w" pour ajouter la nouvelle ligne)
    fichier = fopen("data.txt", "a+"); 
    // Vérifier si l'ouverture du fichier a réussi
    if (fichier == NULL) {
        printf("Erreur lors de l'ouverture du fichier.");
        return -1; // Quitter le programme avec un code d'erreur
    }
    // Écrire dans le fichier
    fprintf(fichier, "%s,%s,%s,%d\n", 
      data->name, data->activity, 
      day_to_string(data->day), data->hour);

    // Fermer le fichier
    fclose(fichier);
    return 0 ; 

}

//Enlève la donnée _data_ de la base de donnée
void delete_data(Data* data) {
  rename("data.txt","ancien_fichier.txt") ; 
  sem_wait(semaphore) ; //attente du sémaphore avant d'ouvrir le fichier, ici le sémaphore est commun pour les deux fichiers, ancien_fichier et data
  FILE* ancien_fichier = fopen("ancien_fichier.txt", "r") ; 
  if (ancien_fichier == NULL) {
    printf("Fichier illisible \n");
    return ; 
  }
  else{
    char data_char [LINE_SIZE]; // char correspondant à la data
    data_format(data_char, data) ; 
    char line [LINE_SIZE] ; //on considère que ça ne fera pas plus
    int test_suppression=0 ; 
    // On parcourt les lignes et on les convertit en data puis on les stocke dans une liste puis après avoir supprimé la data souhaitée on remet tout sous la bonne forme dans le fichier
    while(fgets(line, sizeof(line), ancien_fichier) != NULL )  { 
      if (!(strcmp(line,data_char))) {
        test_suppression = 1 ;    
      }
      else {
        Data* line_data = get_data(line) ; 
        add_data(line_data) ; 
        printf("ligne ajoutée \n") ; 
        data_free(line_data) ; 
      }

    }
    if (test_suppression==1){
      printf("suppression effectuée \n ") ; 
    }
    else {
      printf("activité non existante \n") ; 
    }
    fclose(ancien_fichier) ; 
    sem_post(semaphore); 
    return ; 
  }
}

//Affiche le planning
char* see_all(char* answer) {
  Data* data ;
  sem_wait(semaphore) ; //attente du sémaphore avant d'ouvrir le fichier
  FILE* fichier = fopen("data.txt","r") ; 
  char line[LINE_SIZE] ; 
  while(fgets(line, sizeof(line), fichier) != NULL )  {
    data = get_data(line) ; 
    printf("%s %dh : %s a %s \n", day_to_string(data->day), data->hour, data->name, data->activity ) ; 
    data_free(data) ; 
  }
  fclose(fichier) ; 
  sem_post(semaphore) ; 
  return "terminé" ; 
}

int main(int argc, char** argv) {
    semaphore = sem_open(SEM_NAME, 0) ; //initialisation du sémaphore

    if (semaphore == SEM_FAILED) {exit_msg("semaphore failed", 1) ; }

    //Dans le cas où la commande est ADD
    if (!(strcmp(argv[1],"ADD"))) {
      char line [LINE_SIZE] ; 
      sprintf(line, "%s,%s,%s,%s", argv[2],argv[3],argv[4],argv[5]) ; 
      Data* data = get_data(line) ; 
      sem_wait(semaphore) ; //attente du sémaphore avant add data 
      add_data(data) ; 
      data_free(data) ; 
      sem_post(semaphore) ; //on libère le sémaphore

    }
    //cas où la commande est SEE
    else if (!(strcmp(argv[1],"SEE"))) {
      see_all("hello") ; 
    }
    //Cas où la commande est DEL 
    else if (!(strcmp(argv[1],"DEL"))){
      char line [LINE_SIZE] ; 
      sprintf(line, "%s,%s,%s,%s", argv[2],argv[3],argv[4],argv[5]) ; 
      Data* data = get_data(line) ; 
      delete_data(data) ; 
      data_free(data) ; 
    }
    else {
      printf("commande non reconnue") ;
      return 1 ; // échec de la commande
    }
    //printf("commande effectuée") ; 

    return 0; // Tout s'est bien passé
}

