#include <stdio.h>
#include <string.h>
#include "utils.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <semaphore.h> 
#include <pthread.h>
#include <fcntl.h>


char* bdd_bin = "./bdd";

//On prépare les arguments qui seront envoyés à bdd
//ADD toto poney lundi 3 -> { "./bdd", "ADD", "toto", "poney", lundi", "3", NULL }
char** parse(char* line) {
  char** res = malloc(7 * sizeof(char*));
  res[0] = bdd_bin;

  char* arg1 = strtok(line, " ");
  res[1] = arg1;

  char* arg2 = strtok(NULL, " ");
  res[2] = arg2;
  if (arg2 == NULL) {
    arg1[strlen(arg1)-1] = '\0';
    return res;
  }

  char* arg3 = strtok(NULL, " ");
  res[3] = arg3;
  if (arg3 == NULL) { 
    arg2[strlen(arg2)-1] = '\0';
    return res;
  }

  char* arg4 = strtok(NULL, " ");
  res[4] = arg4;
  if (arg4 == NULL) {
    arg3[strlen(arg3)-1] = '\0';
    return res;
  } 
  
  char* arg5 = strtok(NULL, "\n");
  res[5] = arg5;
  res[6] = NULL;
  return res;
}

//Configuration de la socket réseau, retourne le file descriptor de la socket
int configure_socket() {
  int socket_desc;
  socket_desc = socket(AF_INET, SOCK_STREAM, 0) ; //AF_INET pour IPV4, SOCK_STREAM pour TCP 
  if (socket_desc <0 ) {
    exit_msg("socket", 1 ) ; 
    }
  struct sockaddr_in address ; 
  address.sin_family = AF_INET ; 
  address.sin_addr.s_addr = inet_addr(SERVER_IP);
  address.sin_port = htons(SERVER_PORT) ; 

  if (bind(socket_desc, (struct sockaddr*) &address, sizeof(address))<0) { //erreur dans bind
    exit_msg("bind", 1);
  }
  return socket_desc;
}

// Passage des commandes à la base de données par un pipe
// Renvoi des réponses au client par la socket réseau
void process_communication(int socket_reception) {
    char commande_string [LINE_SIZE] ; 
    char** arguments_commande ; 

    if(recv(socket_reception, commande_string, LINE_SIZE, 0)<0) {exit_msg("recv", 1);} //stocke les commandes dans le buffer commande_string 
    printf("%s \n", commande_string) ; 
    arguments_commande = parse(commande_string) ; 

    // On fork pour lancer le programme bdd
    pid_t pid= fork() ; 
    int fds[2] ; //On crée un tableau pour contenir deux descripteurs de fichiers

    //Création d'un pipe 
    pipe(fds) ;
    if (pid==0) {
      close(fds[0]); 
      dup2(fds[1], STDIN_FILENO) ; 
      execv("./bdd", arguments_commande) ; 
    }
    else if (pid!=0){
      printf("Waiting for the chil\n");
      close(fds[1]) ;
      int status ;
      wait(&status) ; 
      printf("finish \n") ; 
      
      char reply [REPLY_SIZE] ; 
      read(fds[0], reply, sizeof(reply)) ; 
      
      if(send(socket_reception, reply, sizeof(reply), 0)<0) {
        exit_msg("error send", 1) ; 
      }

      if (status!=0) {exit_msg("bbd failed", 1) ; }
      else {printf("Commande effectuée \n") ; }  
    }
}


int main(int argc, char** argv) {
  int socket_desc;
  int new_socket;
  sem_t* semaphore ; 
  semaphore = sem_open(SEM_NAME, O_CREAT|O_EXCL, SEM_MODE, 1) ; //Création du sémaphore   
  if (semaphore == SEM_FAILED) 
  {
    sem_unlink(SEM_NAME) ; 
    exit_msg("semaphore failed : relancer serveur", 1) ; }


  // Configuration de la socket serveur
  socket_desc = configure_socket();

  // Réception des connections réseaux entrantes
  while (1) {                               //en attente d'une commande 
    if(listen (socket_desc, 50)<0) { exit_msg("listen",1) ; } // Gestion de l'erreur du socket
    struct sockaddr_in address ;
    socklen_t len_address = sizeof(address) ;  
    int socket_reception =  accept (socket_desc, (struct sockaddr*) &address, &len_address)  ;          //reçoit un socket 
    if (socket_reception <0) exit_msg("accept", 1);


  // Gestion des commandes entrantes dans la nouvelle socket
  process_communication(socket_reception);
  }

  

  return 0;
  }

